import { Component } from '@angular/core';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../../service/auth.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(
    public router: Router,
    private authService: AuthService) {

  }
  public user = this.authService.usuario ? this.authService.usuario : null
  public isLogged = false


  /*     this.user = await this.authService.getCurentUser()
  if (this.user) {
    this.isLogged = true
  
  }
   */
  get photourl() {
    if (this.user.photoURl) {
      return this.user.photoURl
    } else {
      return '../../../../assets/img/user.png'
    }
  }
  formLogin = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required)
  })
  formRegistro = new FormGroup({
    name: new FormControl('', Validators.required),
    surname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    password: new FormControl('', Validators.required),
    city: new FormControl(''),
    province: new FormControl(''),
  })

  dataUSer = {
    img: ''
  }
  msgLogin = ''
  msgRegistro = {
    msg: '',
    color: '',
    stado: false
  }
  get emailFromLogin() {
    return this.formLogin.get('email')
  }
  get passwordFromLogin() {
    return this.formLogin.get('password')
  }


  sendReset() {
    // Cambia de URL
    document.getElementById('buttonCloseModalIngresar')?.click()
    this.router.navigateByUrl('reset')
  }
  get ismain() {
    return this.router.url == '/'
  }
  get ismenu() {
    return (this.router.url != "/" && this.router.url != '/reset')
  }
  get isreset() {
    return this.router.url == '/reset'
  }
  registarse(formulario: FormGroup) {
    if (formulario.valid) {
      console.log("Formulario valido -----");
      const { email, password } = formulario.value
      this.authService.createUser(email, password)
        .then(() => {
          this.msgRegistro.msg = "Registro exitoso";
          this.msgRegistro.color = "alert-success";
          this.msgRegistro.stado = true
        })
        .catch(err => {
          this.msgRegistro.msg = err;
          this.msgRegistro.color = "alert-danger";
          this.msgRegistro.stado = true;
        })

    } else {
      console.log("El formulario no es valido")
    }
  }
  logearse(form: FormGroup) {
    if (form.valid) {
      this.authService.login(form.get('email')?.value, form.get('password')?.value)
        .then(() => {
          console.log('Login exitoso');
          this.router.navigateByUrl('/menu'),
            document.getElementById('buttonCloseModalIngresar')?.click()
        }).catch(err => this.msgLogin = err)

      console.log("El formulario es valido");
      console.log(form.get('email')?.value);
      this.router.navigateByUrl('menu')
    } else {
      console.log("El formulario no es valido");
    }

  }
  signupGooglepop() {
    this.authService.signupGoogle()
      .then((result) => {
        localStorage.setItem('authgoogle', JSON.stringify(result))
        document.getElementById('buttonCloseModalIngresar')?.click()

        this.router.navigateByUrl('menu')

      })
      .catch(err => console.log(err))
  }
  salir() {

    this.authService.logout()
      .then(() => {
        console.log("Saliendo ");
        this.user = {}
        this.router.navigateByUrl('');
      })
      .catch(err => console.log(err))

  }
}
