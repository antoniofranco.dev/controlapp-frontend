import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';
import { AuthService } from '../../../service/auth.service';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  items: MenuItem[] | any

  constructor(private afa: AuthService) {

  }

  ngOnInit() {
    this.items = [
      {
        label: "Home",
        icon: "pi pi-fw pi-home",
        routerLink: "/menu/"
      },
      {
        label: 'Ticket',
        icon: 'pi pi-fw pi-ticket',
        items: [
          {
            label: 'Nuevo',
            icon: 'pi pi-fw pi-plus',
            routerLink: "/menu/ticket/new"
          },
          {
            label: 'Editar',
            icon: 'pi pi-fw pi-pencil',
            routerLink: "/menu/ticket/edit"
          },
          {
            label: 'Ver',
            icon: 'pi pi-fw pi-info',
            routerLink: "/menu/ticket/view"
          },
          /*           {
                      label: 'Eliminar',
                      icon: 'pi pi-fw pi-trash',
                      routerLink: "/menu/ticket/remove"
                    }, */
        ]
      },
      {
        label: 'Envio',
        icon: 'pi pi-fw pi-dollar',
        items: [
          /*           {
                      label: 'Nacional',
                      icon: 'pi pi-fw pi-money-bill',
                      routerLink: "/menu/envio/nacional"
                    }, */
          {
            label: 'Internacional',
            icon: 'pi pi-fw pi-money-bill',
            routerLink: "/menu/envio/internacional"
          },
        ]
      },
      {
        label: 'Reclamos',
        icon: 'pi pi-fw pi-desktop',
        routerLink: "/menu/dashboard"
      },
      {
        label: 'Perfil',
        icon: 'pi pi-fw pi-user',
        routerLink: "/menu/perfil"
      },
      {
        label: "Salir",
        icon: "pi pi-fw pi-times",
        routerLink: "/",
        command: () => { this.afa.logout().then(exit => "Saliendo").catch(err => console.log(err)) }
      },
    ]
  }
}
