import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './component/header/header.component';
import { FooterComponent } from './component/footer/footer.component';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MainRoutingModule } from '../core/main/main-routing.module';
import { SidebarComponent } from './component/sidebar/sidebar.component';
import { PanelMenuModule } from 'primeng/panelmenu';
import { AvatarModule } from "primeng/avatar";
import { AvatarGroupModule } from 'primeng/avatargroup';


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent],
  imports: [
    CommonModule,
    RouterModule,
    MainRoutingModule,
    ReactiveFormsModule,
    PanelMenuModule,
    AvatarModule,
    AvatarGroupModule
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    SidebarComponent]
})
export class SharedModule { }
