import { Component, OnInit } from '@angular/core';
import { ChartType } from 'chart.js';
import { MultiDataSet, Label, Color } from 'ng2-charts';
import { IncidenceService } from '../../../service/incidence.service';

@Component({
  selector: 'app-HomeGraficas',
  templateUrl: './HomeGraficas.component.html',
  styleUrls: ['./HomeGraficas.component.css']
})
export class HomeGraficasComponent implements OnInit {
  // Doughnut
  valoresDonasReclamo = [0, 0, 0]
  valoresDonasReclamoAgente = [0, 0, 0]

  public labelsAgents: Label[] = ['Martin', "Franco", "Nicolas"]
  public doughnutChartLabels: Label[] = ['Cerrados', 'Abierto', 'Pendiente'];
  public doughnutChartData: MultiDataSet = [
    this.valoresDonasReclamo
  ];
  public doughnutChartType: ChartType = 'doughnut';

  constructor(
    private incidencia: IncidenceService
  ) {

  }

  ngOnInit(): void {
    this.incidencia.getINcidence()
      .subscribe((rta: any) => {
        console.log(rta)
        const open = rta.filter((data: any) => data.state == "open")
        const close = rta.filter((data: any) => data.state == "close")
        const current = rta.filter((data: any) => data.state == "current")


        const martinAgente = rta.filter((data: any) => data.agente == "Martin")
        const francoAgente = rta.filter((data: any) => data.agente == "Franco")
        const nicolasAgente = rta.filter((data: any) => data.agente == "Nicolas")

        console.log(martinAgente.length,
          francoAgente.length,
          nicolasAgente.length);
        this.valoresDonasReclamoAgente = [martinAgente.length,
        francoAgente.length,
        nicolasAgente.length]
        this.valoresDonasReclamo = [close.length, open.length, current.length]
        /*         console.log(this.valoresDonasReclamo);
         */
      }
      )
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }
}