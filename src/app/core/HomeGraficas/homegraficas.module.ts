import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeGraficasComponent } from './homegraficas.component/HomeGraficas.component';
import { graficasRoutingModule } from './homegraficas-routing.module';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    graficasRoutingModule,
    ChartsModule
  ],
  declarations: [HomeGraficasComponent,
  ]
})
export class HomegraficasModule { }
