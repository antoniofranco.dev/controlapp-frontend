
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeGraficasComponent } from './homegraficas.component/HomeGraficas.component';

const routes: Routes = [
  { path: '', component: HomeGraficasComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class graficasRoutingModule { }
