import { Component } from '@angular/core';
import { AuthService } from '../../../../service/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent {

  formReset = new FormGroup({
    email: new FormControl('', [Validators.email, Validators.required])
  }
  )
  alerta = {
    exito: false,
    color: 'alert-success',
    msn: ''
  }
  constructor(
    private authService: AuthService
  ) { }


  get formGroupEmail() {
    return this.formReset.get('email')
  }

  errorEmail() {
    const email = this.formGroupEmail
    return email?.hasError('email') && email.touched
  }

  errorEmailEmpty() {
    const email = this.formGroupEmail
    return email?.hasError('required') && email.touched
  }
  researPassword(formulario: FormGroup) {
    if (formulario.valid) {
      const email = formulario.get('email')?.value
      this.authService.resetPassword(email)
        .then(msg => {
          this.alerta.msn = "El correo para resetear la clave fue enviado correctamente"
          this.alerta.exito = true
        })
        .catch(err => {
          this.alerta.msn = err,
            this.alerta.exito = true
          this.alerta.color = 'alert-danger'
        })
    }
  }
}
