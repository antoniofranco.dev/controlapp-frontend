import { Component, OnInit } from '@angular/core';
import { IncidenceService } from '../../../service/incidence.service';
import { FormBuilder, FormControl, Validator, FormGroup, Validators } from '@angular/forms';
import { Incidence } from '../incidence';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  data2: any
  dataOriginal: any
  formReclamo: any
  constructor(
    private incidenceService: IncidenceService,
    private formBuilder: FormBuilder
  ) {
    this.buildForm()
    this.incidenceService.getINcidence()
      .subscribe((rta: any) => {
        //TODO: Ordenar Array Por fecha
        this.data2 = rta;
        this.dataOriginal = rta
        console.log(this.data2);
      })
  }
  ngOnInit(): void {

  }
  /*   get traerdata() {
      return this.incidenceService.datas
    } */

  filtrarPendiente() {
    console.log("Pendiente");
    this.data2 = this.dataOriginal.filter((data: Incidence) => data.state == "current")
  }
  filtrarOpen() {
    console.log("Open");
    this.data2 = this.dataOriginal.filter((data: Incidence) => data.state == "open")

  }
  filtrarClose() {
    console.log("Close");
    this.data2 = this.dataOriginal.filter((data: Incidence) => data.state == "close")

  }

  enviarData() {
    document.getElementById('IncidenciaModal')?.click()
    const data = this.formReclamo.value
    console.log(data);
    this.incidenceService.sendIncidence(data).subscribe(rta => { console.log(rta); })
    this.formReclamo.reset()
    this.incidenceService.getINcidence()
      .subscribe((rta: any) => {
        this.data2 = rta;
        this.dataOriginal = rta
        console.log(this.data2);
      })


  }
  eliminarRegistro(id: string, modal: string) {
    // Elimina registro for ID
    document.getElementById(modal)?.click()
    console.log("Eliminar Registro ");
    this.incidenceService.deleteIncidence(id)

  }
  putData(i: any, id: string) {
    console.log(`i = ${i}`);
    console.log(id);
    const dataObservaciones = <HTMLInputElement>document.getElementById(`${i}observaciones`)
    const dataSolucion = <HTMLInputElement>document.getElementById(`${i}solucion`)
    const dataSelect = <HTMLInputElement>document.getElementById(`${i}select`)

    const objeto = {
      "observations": dataObservaciones.value,
      "solucion": dataSolucion.value,
      "state": dataSelect.value
    }
    console.log(objeto);
    console.log(id);
    this.incidenceService.putIncidence(id, objeto)
  }


  private buildForm() {
    this.formReclamo = this.formBuilder.group(
      {
        date: ['', Validators.required],
        terminal: ['', Validators.required],
        agente: ['', Validators.required],
        reclamo: ['', Validators.required],
        observations: ['', Validators.required],
        solucion: [''],
        state: ['open']
      }

    )
  }
  downloadFile(data: any) {
    const replacer = (key: any, value: any) => (value === null ? '' : value); // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    const csv = data.map((row: any) =>
      header
        .map((fieldName) => JSON.stringify(row[fieldName], replacer))
        .join(',')
    );
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const a = document.createElement('a');
    const blob = new Blob([csvArray], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = 'myFile.csv';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }
}
