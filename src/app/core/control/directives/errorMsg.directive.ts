import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[error-msg]'
})
export class ErrorMsgDirective implements OnInit {
  htmlElemento: ElementRef<HTMLElement>
  @Input() color: string = "red";
  @Input() mensaje: string = ""
  colorClass: string = 'bg-primary'
  constructor(private el: ElementRef<HTMLElement>) {

    this.htmlElemento = el
  }
  ngOnInit(): void {
    this.setTexto()
    switch (this.mensaje) {
      case "close":
        this.colorClass = "bg-danger"
        break
      case "open":
        this.colorClass = "bg-primary"
        break
      case "current":
        this.colorClass = "bg-warning"
        break
      case "null":
        this.colorClass = "bg-dark"
        break
    }
    this.setColor()
  }
  setColor(): void {
    this.htmlElemento.nativeElement.classList.add(this.colorClass)
  }
  setTexto(): void {
    this.htmlElemento.nativeElement.innerText = this.mensaje
  }
}
