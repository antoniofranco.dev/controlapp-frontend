import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ControlRoutingModule } from './contro-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { EstadoPipe } from './pipes/Estado.pipe';
import { ErrorMsgDirective } from './directives/errorMsg.directive';
import { EstadoDirective } from './directives/Estado.directive';



@NgModule({
  declarations: [
    DashboardComponent,
    EstadoPipe,
    ErrorMsgDirective,
    EstadoDirective],
  imports: [
    CommonModule,
    ControlRoutingModule,
    ReactiveFormsModule
  ]
})
export class ControlModule { }
