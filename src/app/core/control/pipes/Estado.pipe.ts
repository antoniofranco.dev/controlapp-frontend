import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'Estado'
})
export class EstadoPipe implements PipeTransform {

  transform(value: string, args?: any): string {
    return `${value}`;
  }

}
