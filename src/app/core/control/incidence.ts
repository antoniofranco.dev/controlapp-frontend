export interface Incidence {

    date: string
    time: string
    terminal: string
    observations: string,
    state: string
    agente: string,
    solucion: string
}
