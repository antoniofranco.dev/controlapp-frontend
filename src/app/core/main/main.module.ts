import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';


import { MainComponents } from './pages/home/home.component';
import { SharedModule } from '../../shared/shared.module';
import { MenuComponent } from './pages/menu/menu.component';
import { LoadComponent } from './pages/load/load.component';

import { TicketRoutingModule } from '../ticket/ticket-routing.module';
import { ControlRoutingModule } from '../control/contro-routing.module';



@NgModule({
  declarations: [
    MainComponents,
    MenuComponent,
    LoadComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    MainRoutingModule,
    TicketRoutingModule,
    ControlRoutingModule
  ],
  exports: [MainComponents]
})
export class MainModule { }
