import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MenuComponent } from './pages/menu/menu.component';
import { LayoutComponent } from '../home/layout/layout.component';




const routes: Routes = [

    {
        path: 'menu',
        component: LayoutComponent,
        children: [
            {
                path: '',
                component: MenuComponent,
                loadChildren: () => import('../HomeGraficas/homegraficas.module')
                    .then(m => m.HomegraficasModule)
            },
            {
                path: 'ticket',
                loadChildren: () => import('../ticket/ticket.module')
                    .then(m => m.TicketModule)
            },
            {
                path: 'perfil',
                component: MenuComponent,
                loadChildren: () => import('../perfil/perfil.module')
                    .then(m => m.PerfilModule)

            },
            {
                path: 'envio',
                loadChildren: () => import('../envio/envio.module')
                    .then(m => m.EnvioModule)

            },
            {
                path: 'dashboard',
                component: MenuComponent,
                loadChildren: () => import('../control/control.module')
                    .then(m => m.ControlModule)
            }
        ]
    }
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class MainRoutingModule { }