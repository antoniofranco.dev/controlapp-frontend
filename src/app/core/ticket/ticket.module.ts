import { NgModule } from '@angular/core';
import { CommonModule, CurrencyPipe } from '@angular/common';
import { LoadComponent } from './load/load.component';
import { EditComponent } from './edit/edit.component';
import { RemoveComponent } from './remove/remove.component';
import { ViewComponent } from './view/view.component';
import { TicketRoutingModule } from './ticket-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    LoadComponent,
    EditComponent,
    RemoveComponent,
    ViewComponent,
  ],
  imports: [
    CommonModule,
    TicketRoutingModule,
    SharedModule,
    ReactiveFormsModule,

  ], providers: [CurrencyPipe]
})
export class TicketModule { }
