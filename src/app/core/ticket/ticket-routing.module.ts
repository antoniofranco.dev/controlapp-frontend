import { Routes, RouterModule } from '@angular/router';
import { NgModule, Component } from '@angular/core';
import { LoadComponent } from './load/load.component';
import { EditComponent } from './edit/edit.component';
import { ViewComponent } from './view/view.component';
import { RemoveComponent } from './remove/remove.component';
import { LayoutComponent } from '../home/layout/layout.component';
import { MenuComponent } from '../main/pages/menu/menu.component';





const routes: Routes = [
    {
        path: '',
        component: MenuComponent,
        children: [
            { path: 'new', component: LoadComponent },
            { path: 'edit', component: EditComponent },
            { path: 'edit/:id', component: EditComponent },
            { path: 'view', component: ViewComponent },
            { path: 'remove', component: RemoveComponent },
        ]
    }
]


@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TicketRoutingModule { }