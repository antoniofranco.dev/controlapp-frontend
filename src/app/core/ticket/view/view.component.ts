import { Component, OnInit } from '@angular/core';
import { TicketService } from 'src/app/service/ticket.service';
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.css']
})
export class ViewComponent implements OnInit {

  datas: [] | any
  original: [] | any
  get resultadoData() {
    return this.ticketService.datas
  }
  constructor(
    private ticketService: TicketService
  ) {
    ticketService.getTicket()
      .subscribe(
        (resp: any) => {
          this.original = resp
          this.datas = this.original
          console.log(this.datas);
        }
      )
  }
  filtrarTerminal(terminal: string) {
    // this.datas = this.datas.filter((data: any) => data.termial == "A07929")
    console.log(this.datas);
    console.log(this.original);
    this.datas = this.original.filter((data: any) => data.termial == terminal)
  }
  ngOnInit(): void {
  }
  traerticket() {
    this.ticketService.getTicket()
  }
  eliminarRegistro(id: string, modal: string) {
    console.log(id);
    document.getElementById(modal)?.click()
    this.ticketService.deleteData(id)
  }


  downloadFile(data: any) {
    const replacer = (key: any, value: any) => (value === null ? '' : value); // specify how you want to handle null values here
    const header = Object.keys(data[0]);
    const csv = data.map((row: any) =>
      header
        .map((fieldName) => JSON.stringify(row[fieldName], replacer))
        .join(',')
    );
    csv.unshift(header.join(','));
    const csvArray = csv.join('\r\n');

    const a = document.createElement('a');
    const blob = new Blob([csvArray], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);

    a.href = url;
    a.download = 'myFile.csv';
    a.click();
    window.URL.revokeObjectURL(url);
    a.remove();
  }
}
