import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router'
import { TicketService } from '../../../service/ticket.service';
import { Ticket } from '../interface/ticket';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  form: any = ''

  datos: Ticket | any
  constructor(
    private rutaActiva: ActivatedRoute,
    private ticketService: TicketService,
    private formBuilder: FormBuilder,

  ) {
    this.buildForm()
  }
  get publicId() {
    return this.rutaActiva.snapshot.params.id
  }
  ngOnInit(): void {
    if (this.rutaActiva.snapshot.params.id) {
      this.ticketService.getTicketId(this.rutaActiva.snapshot.params.id)
        .subscribe(
          (rta) => {
            this.datos = rta
            console.log(rta);
          })
    }
  }
  private buildForm() {
    this.form = this.formBuilder.group(
      {
        date: ['',],
        time: ['',],
        termial: ['TerminalByData'],
        cobEfec: [''],
        cobEfecCant: [''],
        cobPei: [''],
        cobPeiCant: [''],
        cobPendEfect: [''],
        cobPendEfectCant: [''],
        cobPendPei: [''],
        cobPendPeiCant: [''],
        paidEfec: [''],
        paidEfecCant: [''],
        paidPei: [''],
        paidPeiCant: [''],
        totalGeneral: [''],
        numberSecuencia: ['', Validators.required]
      }
    )
  }
  Enviar(evento: any) {

  }

}
