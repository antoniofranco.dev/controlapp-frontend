import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { CurrencyPipe } from '@angular/common';
import { TicketService } from 'src/app/service/ticket.service';


@Component({
  selector: 'app-load',
  templateUrl: './load.component.html',
  styleUrls: ['./load.component.css']
})
export class LoadComponent implements OnInit {
  dateToday: Date = new Date()




  form: any = ''

  constructor(
    private formBuilder: FormBuilder,
    private ticketService: TicketService) {
    this.buildForm()
  }

  ngOnInit(): void {
/*     console.log(this.form.get('termial').value);
 */  }


  Enviar(event: any) {
    if (this.form.valid) {

      console.log(this.form.value);
      console.log(this.form.valid);
      this.SendataApiPost()

    }
  }

  private SendataApiPost() {
    const data = this.form.value
    this.form.reset()

    this.ticketService.CreateTicket(data)
      .subscribe(rta => {
        console.log(rta)
      })
  }
  // Geters


  get terminalNotPei() {
    return this.form.get('termial').value != 'A07929'
  }

  private buildForm() {
    this.form = this.formBuilder.group(
      {
        date: [``, Validators.required],
        time: ['', Validators.required],
        termial: ['A13491', Validators.required],
        cobEfec: [''],
        cobEfecCant: [''],
        cobPei: [''],
        cobPeiCant: [''],
        cobPendEfect: [''],
        cobPendEfectCant: [''],
        cobPendPei: [''],
        cobPendPeiCant: [''],
        paidEfec: [''],
        paidEfecCant: [''],
        paidPei: [''],
        paidPeiCant: [''],
        totalGeneral: [''],
        numberSecuencia: ['', Validators.required]
      }
    )

  }
}
