import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EnvioNacionalComponent } from './envio-nacional/envio-nacional.component';
import { NacionalComponent } from './nacional/nacional.component';
import { InternacionalComponent } from './internacional/internacional.component';
import { EnvioRoutingModule } from './envio-routing.module';



@NgModule({
  declarations: [
    EnvioNacionalComponent,
    NacionalComponent,
    InternacionalComponent],
  imports: [
    CommonModule,
    EnvioRoutingModule,
  ]
})
export class EnvioModule { }
