import { Component, OnInit, NgModule } from '@angular/core';

@Component({
  selector: 'app-internacional',
  templateUrl: './internacional.component.html',
  styleUrls: ['internacional.styles.css']
})
export class InternacionalComponent implements OnInit {
  inputPrincipal: number = 0
  inputCargos: number = 0
  inputIva: number = 0
  inputTotal: number = 0

  constructor() { }

  ngOnInit(): void {
  }

  calcular(event: any) {
    this.inputPrincipal = parseFloat(event.target.value)
    this.inputCargos = this.sacarPorciento(this.inputPrincipal, 5)

    this.inputIva = this.sacarPorciento(this.inputCargos, 21)

    this.inputTotal = this.inputPrincipal + this.inputCargos + this.inputIva


  }
  sacarPorciento(valor: number, numero: number): number {

    return parseFloat(((valor * numero) / 100).toFixed(2))
  };



}
