import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { MenuComponent } from '../main/pages/menu/menu.component';
import { NacionalComponent } from './nacional/nacional.component';
import { InternacionalComponent } from './internacional/internacional.component';




const routes: Routes = [
    {
        path: '',
        component: MenuComponent,
        children: [
            {
                path: "nacional",
                component: NacionalComponent
            },
            {
                path: 'internacional',
                component: InternacionalComponent
            }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EnvioRoutingModule { }