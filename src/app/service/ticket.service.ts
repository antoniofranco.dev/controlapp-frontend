import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Ticket } from '../core/ticket/interface/ticket';
import { query } from '@angular/animations';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class TicketService {

  // Cambiar any por su interface
  public datas: any[] = []
  constructor(
    private http: HttpClient
  ) { }


  CreateTicket(data: Ticket) {
    return this.http.post(`${environment.URL}api/ticket`, data)
  }
  getTicket(query: string = '') {
    query = query.trim().toLocaleLowerCase()
    const params = new HttpParams()
      .set('terminal', 'all')

    return this.http.get(`${environment.URL}api/ticket`, { params })
  }

  /*  deleteData(data: string) {
     this.http.delete(`${environment.URL}api/ticket/${data}`)
       .subscribe((rta: any) => {
         console.log(rta);
       })
   } */
  getTicketId(data: string) {
    return this.http.get(`${environment.URL}/api/ticket/${data}`)
  }

}
