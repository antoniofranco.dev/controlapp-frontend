import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Incidence } from '../core/control/incidence';
import { Data } from '@angular/router';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IncidenceService {
  datas: any = ''
  constructor(private http: HttpClient) {
  }

  sendIncidence(data: Incidence) {
    return this.http.post(environment.URL + 'api/incidence', data)
  }

  getINcidence() {
    return this.http.get(environment.URL + 'api/incidence')

  }
  deleteIncidence(id: string) {
    this.http.delete(`${environment.URL}api/incidence/${id}`)
      .subscribe((rta: any) => {
        /*     console.log(rta); */
      })
  }
  putIncidence(id: string, data: Object) {
    console.log(id);
    console.log(data);
    this.http.put(`${environment.URL}api/incidence/${id}`, data)
      .subscribe(/* (rta) => console.log(rta) */)

  }
}
