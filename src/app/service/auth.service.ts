import { Injectable, Pipe } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { first, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false
  usuario: any = {};
  constructor(
    private afa: AngularFireAuth
  ) {
    this.afa.authState.subscribe(user => {
      if (!user) {
        return
      }
      this.usuario.nombre = user.displayName
      this.usuario.uid = user.uid
      this.usuario.email = user.email
      this.usuario.photoURl = user.photoURL
      this.usuario.emailVerified = user.emailVerified
      this.usuario.providerId = user.providerData[0]?.providerId

    })
  }

  user: any = '';
  createUser(email: string, password: string) {
    return this.afa.createUserWithEmailAndPassword(email, password)
  }


  async login(email: string, password: string) {
    try {
      const result = await this.afa.signInWithEmailAndPassword(email, password)
        .then(res => {
          this.isLoggedIn = true,
            localStorage.setItem('user', JSON.stringify(res.user))
        });
      return result

    } catch (error) {
      console.log(error);
    }
  }
  async signup(email: string, password: string) {
    try {
      const result = await this.afa.createUserWithEmailAndPassword(email, password)
        .then(res => {
          this.isLoggedIn = true,
            localStorage.setItem('user', JSON.stringify(res.user))
        })
      return result

    } catch (error) {
      console.log(error);
    }
  }

  signupGoogle() {
    const provider = new firebase.auth.GoogleAuthProvider()
    return this.afa.signInWithPopup(provider)
  }
  currentUser() {
    return this.afa.currentUser
  }
  getCurentUser() {
    // return this.afa.authState.pipe(first()).toPromise()
    return this.afa.authState.pipe(tap(resp => console.log("authService", resp)))
  }

  resetPassword(emailReset: string) {
    return this.afa.sendPasswordResetEmail(emailReset, { url: 'http://localhost:4200/' })
  }

  async logout() {
    try {
      // salir
      localStorage.removeItem('user')
      await this.afa.signOut()
    } catch (error) {
      console.log(error);
    }

  }


  changePassword() {
    // TODO Implementar el servicio de cambio de contraseña
    return ""
  }
}
